export interface Telefonos
{
    idTelefono?: string;
    NumTelefono: string;
    parentesco: string;    
}


/**
 * 
 * Creacion de telefonos 
 * 
 * Codigo JSON con metodo post en la ruta /alumnos/telefono
 * 
 * *****************************************************
 * 
        {
            "NumTelefono": "351925672",
            "parentesco": "propio"    
        }
 *
 * *****************************************************
 */