export interface Profesores
{
    idProfesores?: string;
    Telefonos_idTelefonos: string;
    Egresos_idEgresos: string;
    Persona_idPersona: string;
}

/**
 * 
 *  JSON Para agregar un nuevo profesor en la ruta /profesores/nuevoProfesor
 *  con el metodo POST
 * 
 *  ***************************************************
 * 
    {
    "Telefonos_idTelefonos": 2,
    "Egresos_idEgresos": 1,
    "Persona_idPersona": 2
    }
 
 * 
 * *****************************************************
 */