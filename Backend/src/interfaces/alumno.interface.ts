export interface Alumnos
{
    idAlumnos?: string;
    CURP: string;
    Estatura: string;
    Peso: string;
    TipoSangre: string;
    NombreTutor: string;
    NombrePapa: string;
    NombreMama: string;
    TallaAlum: string;
    TallaCintura: string;
    Descuentos_idDescuentos: string;
    Telefonos_idTelefonos: string;
    Persona_idPersona: string;
}


/** 
 * 
 * Creación de JSON para crear alumno con metodo POST 
 * en la ruta /alumnos
    {
    "CURP": "LOYM8450LASDPR0",
    "Estatura": 175,
    "Peso": 80,
    "TipoSangre": "A+",
    "NombreTutor": null,
    "NombrePapa": null,
    "NombreMama": null,
    "TallaAlum": "M",
    "TallaCintura": 90,
    "Descuentos_idDescuentos": 1,
    "Telefonos_idTelefonos": 1,
    "Persona_idPersona": 1
    }
 */