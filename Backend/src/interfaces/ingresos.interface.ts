export interface Ingresos
{
    idIngresos?: string;
    FechaIngreso: Date;
    TipoIngreso: string;
    Descripcion: string;
    Cantidad: string;
    Alumnos_idAlumnos: string;
    
}

/** 
 * 
 * 
 *  JSON Para crear un nuevo ingreso por metodo post en la ruta /ingresos/nuevoIngreso
 * 
      {
        "TipoIngreso": "mensualidad",
        "Descripcion": "Pago del mes Noviembre",
        "Cantidad": 450.00,
        "Alumnos_idAlumnos": 1
      }
*/