export interface Areas
{
    idAreas?: string;
    CapacidadDePersonas: string;
    TipoPiso: string;
    NombreArea: string;
}

/**
 * 
 * 
 *  en la ruta de /areas/nuevaArea con el metodo POST para poder agregar a la base de datos
 *  la estructura de JSON es la siguiente
 * 
   {    
	"CapacidadDePersonas": "15",
     "TipoPiso": "Tatami",
     "NombreArea": "Taekwando"
   }
   
 */