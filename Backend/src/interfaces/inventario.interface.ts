export interface Inventario
{
    TipoMaterial: string;
    Insumos: string;
    Areas_idAreas: string;
}

/**
 *  POST a la tabla de Inventario en la ruta /inventario/nuevoInventario

    {    
        "TipoMaterial": "Pelotas",
        "Insumos": "20",
        "Areas_idAreas": 1
    }
 
 *
 */