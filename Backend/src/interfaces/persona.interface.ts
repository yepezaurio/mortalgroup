export interface Persona
{
    idPersona?: string;
    NombreP: string;
    ApellidoPa :string;
    ApellidoMa :string;
    Calle :string;
    Numero: string;
    Colonia: string;
    Municipio: string;
    CP: string;
    NumSeg: string;
    FechaNacimiento: Date;
}

/** 
 *
 *  Código JSON para insertar una persona en la ruta /alumnos/persona
 * 
 * ***************************************
        {
            "NombreP": "Fulanito",
            "ApellidoPa": "Perengano",
            "ApellidoMa": "Yepez",
            "Calle": "Acacia",
            "Numero": "965",
            "Colonia": "Paseos del Bosque",
            "Municipio": "Zamora",
            "CP": 59695,
            "NumSeg": "595625634",
            "FechaNacimiento": "1998-08-28"
        }
* ****************************************

 */