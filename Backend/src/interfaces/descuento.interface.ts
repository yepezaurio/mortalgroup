export interface Descuentos
{
    idDescuentos?: string;
    DescripcionDescuento: string;
    NombreDescuento: string;
    CantidadDescuento: string;
}

/**
 * 
 * Creacion de un Descuento con el metodo POST en la ruta /alumnos/descuento
 * 
 * 
 *  {
        "DescripcionDescuento": "Inscripcion normal",
        "NombreDescuento": "Sin Descuento",
        "CantidadDescuento": 0.0 
    } 
 
*/