export interface egresos
{
    idEgresos?: string;
    TipoEgreso: string;
    FechaEgreso: Date;
    Monto: number;
    Descripcion: string;
}
/**
 * 
 *  Código tipo JSON solo para copiar y pegar y hacer pruebas en insomnia en el metodo POST 
 *  hacia la tabla de egresos.
 * 
 * **************************************************
        {
            "TipoEgreso": "Renta",
            "Monto": 350,
            "Descripcion": "Pago de renta del lugar"
        }
* ***************************************************
*/