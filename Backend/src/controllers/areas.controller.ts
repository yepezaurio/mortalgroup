import { Request, Response } from 'express';
import { connect } from '../database';
import { Areas } from '../interfaces/areas.interface';

export async function postArea(req:Request, res: Response)
{
    const newArea: Areas = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Areas SET ? ', [newArea]);
    res.json({
        message: 'Se ha creado un Area con exito'
    })
}