import { Request, Response } from 'express';
import { connect } from '../database';
import { egresos } from '../interfaces/egresos.interface';


export async function postEgreso(req: Request, res: Response)
{
    const newEgreso: egresos = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Egresos SET ?', [newEgreso]);
    return res.json({
        message: 'Se ha creado un nuevo egreso o pago'
    });
}