import { Request, Response } from 'express';
import { connect } from '../database';
import { Horario } from '../interfaces/horario.interface';


export async function postHorario(req: Request, res: Response)
{
    const newHorario: Horario = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Horarios SET ?', [newHorario]);
    res.json({
        message: 'Se ha creado con exito'
    });
}


/**
 * 
 * 
 * insercion a la bd por el metodo post con un archivo json a la ruta /horario/nuevoHorario
 * 
  	{    
    "HoraClase": "16:00",
    "DiasClase": "lunes, miercoles y jueves",
    "Profesores_idProfesores": 1
    }
 * 
 * 
 */