import { Request, Response } from 'express';
import { connect } from '../database';
import { Clases } from '../interfaces/clases.interface';

export async function postClase(req: Request, res: Response)
{
    const newClase: Clases = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Clases SET ?', [newClase]);
    res.json({
        message: 'Se ha creado con exito la clase'
    })
}