import{Response, Request} from 'express'
import{connect} from '../database'
import {DetalleMaterial} from '../interfaces/DetalleMaterial.interface'; 
import {Disciplinas} from '../interfaces/Disciplinas.interface'


export async function postDetalleMaterial (req: Request, res: Response)
{
const newDetalleMaterial: DetalleMaterial = req.body;
const conn = await connect();
conn.query('INSERT INTO DetalleMaterial SET ?', [newDetalleMaterial]);
return res.json({
    message: 'Este es el detalle Material'
})
 
}

export async function postDisciplina (req: Request, res: Response)
{
const newDisciplina: Disciplinas = req.body;
const conn = await connect();
conn.query('INSERT INTO disciplinas SET ?', [newDisciplina]);
return res.json({
    message: 'Se ha creado la Disciplina'
})
 
}