import { Request, Response } from 'express'
import { connect } from '../database';
import { Persona  } from '../interfaces/persona.interface';
import { Usuario} from '../interfaces/usuario.interface';
import jws from 'jsonwebtoken';

//SingUp
export async function postUsuario (req: Request, res: Response): Promise <any>
{
const newUsuario: Usuario = req.body;
const crypto = require('crypto');
newUsuario.Nombre = crypto.createHash('md5').update(newUsuario.Nombre).digest('hex');
newUsuario.Contrasena = crypto.createHash('md5').update(newUsuario.Contrasena).digest('hex');
const conn = await connect();
conn.query('INSERT INTO Usuario SET ?', [newUsuario]);

//token
const token: string = jws.sign({ _id: newUsuario.idUsuario }, process.env.TOKEN_USER || 'tokenprueba');

return res.header('authorization', token).json('Usuario Creado con Exito')
 
}

export async function postPersona(req: Request, res: Response)
{
    const newPersona: Persona = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Persona SET ?', [newPersona]);
    return res.json({
        message: 'Sa ha creado una persona con exito'
    })

}

// Este metodo Devuelve todas las Usuario.

//SignIn
export async function getUsuario(req: Request, res:Response): Promise<Response>
{
    const conn = await connect();
    const usuario = await conn.query('Select * Usuarios');
    return res.json(usuario[0]);
} 



