import { Request, Response } from 'express';
import { connect } from '../database';
import { Inventario } from '../interfaces/inventario.interface';

export async function postInventario(res: Response, req: Request)
{
    const newProducto: Inventario = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Inventarios SET ?', [newProducto]);
    res.json({
        message: 'Se ha añadido al inventario'
    })
}

