import { Response, Request } from 'express'
import { connect } from '../database'
import {Clases_has_Disciplinas} from '../interfaces/Clases_has_Disciplina.interface';



export async function postClases_has_Disciplinas (req: Request, res: Response)
{
    const newClases_has_Disciplinas: Clases_has_Disciplinas = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Clases_has_Disciplinas SET ?', [newClases_has_Disciplinas]);
    return res.json({
        message: 'la clase funciona'
    })
}


