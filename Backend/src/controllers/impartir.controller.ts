import{Response, Request} from 'express'
import{connect} from '../database'
//import{Profesores} from '../interfaces/Profesores';
import{ Impartir } from '../interfaces/impartir.interface';

export async function postImpartir (req: Request, res: Response)
{
const newImpartir: Impartir = req.body;
const conn = await connect();
conn.query('INSERT INTO Impartir SET ?', [newImpartir]);
return res.json({
    message: 'Se ha podido asignar clase'
})
 
}
/** 
 export async function postDisciplina (req: Request, res: Response)
{
const newDisciplina: Disciplinas = req.body;
const conn = await connect();
conn.query('INSERT INTO Disciplina SET ?', [newDisciplina]);
return res.json({
    message: 'Se ha creado el Usuario'
})
 
}



*/

/** 
 export async function postProfesor (req: Request, res: Response)
{
const newProfesor: Profesor = req.body;
const conn = await connect();
conn.query('INSERT INTO Profesor SET ?', [newProfesor]);
return res.json({
    message: 'Se ha creado el Usuario'
})
 
}
*/

