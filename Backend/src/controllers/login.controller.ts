import { Request, Response } from 'express';
import { connect } from     '../database';
import jwt from 'jsonwebtoken'


// La funcion para registrar está en Usuarios, por que allá se dan de altá


//Funcion para Hacer un Logeo en el Login.html 
export async function postLogin(req: Request, res: Response): Promise<any>
{

    const conn = await connect();
    const crypto = require('crypto');
    req.body.Nombre = crypto.createHash('md5').update(req.body.Nombre).digest('hex');
    req.body.Contrasena = crypto.createHash('md5').update(req.body.Contrasena).digest('hex');
    const Logeo = await conn.query('SELECT * from Usuario Where Nombre= ? and Contraseña = ? ',[req.body.Nombre, req.body.Contrasena]);
    const JSON1 = JSON.parse(JSON.stringify(Logeo));
    const dato = JSON1[0].length
    if (dato == 0 ) {
        return res.status(400).json('Usuario o Contraseña Invalidos');
    } else {
        const token : string = jwt.sign({ id : JSON1.idUsuario }, process.env.TOKEN_USERS || 'tokenprueba', {
            expiresIn: 60 * 60 * 24
        } )

        return res.header('authorization', token).json({token});
    }
    
}