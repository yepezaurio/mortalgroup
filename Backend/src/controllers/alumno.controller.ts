import { Request, Response } from 'express';
import { connect } from '../database';
import { Persona } from '../interfaces/persona.interface';
import { Telefonos } from '../interfaces/telefonos.interface';
import { Descuentos } from '../interfaces/descuento.interface';
import { Alumnos } from '../interfaces/alumno.interface';



//Crea los datos de un Alumno
export async function postAlumno(req: Request, res: Response)
{
    const newAlumno: Alumnos = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Alumnos SET ?', [newAlumno]);
    return res.json({
        message: 'Se ha creado alumno con exito'
    })
}



//Crea los datos de una Persona.
export async function postPersona(req: Request, res: Response)
{
    const newPersona: Persona = req.body
    const conn = await connect();
    conn.query('INSERT INTO Persona SET ?', [newPersona]);
    const idPersona: any = await conn.query('select idPersona from Persona order by idPersona desc limit 1');
    return res.json(idPersona[0]);
}


// Crea un Telefono.
export async function postTelefono(req: Request, res:Response)
{
    const newTelefono: Telefonos = req.body
    const conn = await connect();
    await conn.query('INSERT INTO Telefonos SET ?', [newTelefono]);
    const idTelefono: any = await conn.query('select idTelefonos from Telefonos order by idTelefonos desc limit 1;')
    return res.json(idTelefono[0])
}

// Crea un descuento 
export async function postDescuento(req: Request, res:Response)
{
    const newDescuento: Descuentos = req.body;
    const conn = await connect();
    conn.query('insert into Descuentos SET ?', [newDescuento]);
    return res.json({
        message: 'Se ha creado un Descuento con exito '
    })
}



// Este metodo Devuelve todas las personas.
export async function getAlumnos(req: Request, res:Response): Promise<Response>
{
    const conn = await connect();
    const alumnos = await conn.query('Select * From Persona ');
    return res.json(alumnos[0]);
}

export async function getDescuentos(req: Request, res: Response): Promise<any> {
    
    const conn = await connect();
    const nombreDesc = await conn.query('Select * from Descuentos');
    return res.json(nombreDesc[0]);
}