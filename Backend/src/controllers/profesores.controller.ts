import { Request, Response } from 'express';
import { connect } from '../database';
import { Profesores } from '../interfaces/profesores.interface';

export async function postProfesores(req: Request, res: Response)
{
    const newProfesor: Profesores = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Profesores SET ? ', [newProfesor]);
    res.json({
        message: 'Profesor Creado con exito'
    })
}