import{Response, Request} from'express'
import{connect} from '../database';
import{Disciplinas} from '../interfaces/Disciplinas.interface';

export async function postDisciplina (req: Request, res: Response)
{
const newDisciplina: Disciplinas = req.body;
const conn = await connect();
conn.query('INSERT INTO disciplinas SET ?', [newDisciplina]);
return res.json({
    message: 'Se ha creado la Disciplina'
})
 
}