import { Request , Response} from 'express';
import { connect } from '../database'
import { Ingresos } from '../interfaces/ingresos.interface'

export async function postIngreso(req: Request, res: Response)
{
    const newIngreso: Ingresos = req.body;
    const conn = await connect();
    conn.query('INSERT INTO Ingresos SET ?', [newIngreso]);
    res.json({
        message: 'Ingreso creado con exito'
    })
}