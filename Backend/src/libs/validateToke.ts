
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';


export interface IPayload {
    id: string;
    iat: number;
} 

export async function TokenValidation (req: Request, res: Response, next: NextFunction)  {
    try {
		if (!req.headers.authorization) {
			return res.status(401).send('Acceso denegado!');
		}
		let token = req.headers.authorization.split(' ')[1];
		if (token === 'null') {
			return res.status(401).send('Acceso denegado!');
		}
		const payload = await jwt.verify(token, process.env.TOKEN_USERS ||'tokenprueba') as IPayload;
		if (!payload) {
			return res.status(401).send('Acceso denegado!');
		}
		req.idUsuario = payload.id;
		
		next();
	} catch(e) {
		//console.log(e)
		return res.status(401).send('Acceso denegado!');
	}
}