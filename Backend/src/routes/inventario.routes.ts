import { Router } from 'express';
const router = Router();

import { postInventario } from '../controllers/inventario.controller';

router.route('/nuevoInventario')
    .post(postInventario)

export default router;