import { Router } from 'express';
const router = Router();

import { postProfesores } from '../controllers/profesores.controller';

router.route('/nuevoProfesor')
    .post(postProfesores)

export default router;