import { Router } from 'express';
const router = Router();

import { getDescuentos ,postDescuento, postPersona, getAlumnos, postTelefono, postAlumno } from '../controllers/alumno.controller';

router.route('/')
    .post(postAlumno)
    //Este obtiene ahorita a todas las personas es un metodo que nos puede servir más tarde
    //En caso de que no Borrar
    .get(getAlumnos)
    
   

router.route('/persona')
    .post(postPersona);
    
router.route('/telefono')    
    .post(postTelefono);


router.route('/descuento')
    .post(postDescuento)
    .get(getDescuentos);

export default router;