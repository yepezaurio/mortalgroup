import { Router } from 'express';
const router = Router();

import { postEgreso } from '../controllers/egresos.controller';

router.route('/')
    .post(postEgreso)

export default router;