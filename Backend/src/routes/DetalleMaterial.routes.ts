import {Router} from 'express';
const router = Router ();

import {postDetalleMaterial} from '../controllers/DetalleMaterial.controller';

router.route ('/')
    .post(postDetalleMaterial)

export default router;