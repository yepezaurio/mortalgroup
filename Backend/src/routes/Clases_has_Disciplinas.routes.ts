import {Router} from 'express';
const router = Router ();

import  {postClases_has_Disciplinas} from '../controllers/Clases_has_Disciplinas.controller'

router.route ('/')
    .post(postClases_has_Disciplinas)

export default router;