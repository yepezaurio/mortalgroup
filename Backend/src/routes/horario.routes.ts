import { Router } from 'express';
const router = Router();

import { postHorario } from '../controllers/horario.controller';

router.route('/nuevoHorario')
    .post(postHorario)

export default router;