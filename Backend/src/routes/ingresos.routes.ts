import { Router } from 'express';
const router = Router();

import { postIngreso } from '../controllers/ingresos.controller';

router.route('/nuevoIngreso')
    .post(postIngreso);

export default router;