import { Router } from 'express';
const router = Router();

import { postClase } from '../controllers/clases.controller'

router.route('/nuevaClase')
    .post(postClase)

export default router;