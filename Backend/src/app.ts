import express, { Application, Request, Response, NextFunction } from 'express';
import morgan, { token } from 'morgan';

//Routes Importante para saber las rutas 
import IndexRoutes from './routes/index.routes'
import LoginRoutes from "./routes/login.routes";
import AlumnoRoutes from './routes/alumno.routes';
import UsuarioRoutes from './routes/usuario.routes';
import EgresoRoutes from './routes/egresos.routes';
import IngresoRoutes from './routes/ingresos.routes';
import ProfesorRoutes from './routes/profesores.routes';
import AreaRoutes from './routes/areas.routes';
import HorarioRoutes from './routes/horario.routes';
import InventarioRoutes from './routes/inventario.routes';
import ImpartirRoutes from './routes/impartir.routes';
import DisciplinaRoutes from './routes/Disciplinas.routes';
import ClasesRoutes from './routes/clases.routes';
import DetalleMaterial from './routes/DetalleMaterial.routes';
import Clases_has_Disciplinas from './routes/Clases_has_Disciplinas.routes';
import { TokenValidation } from "./libs/validateToke";



export class App
{

    private app: Application;


    constructor(private port?: number | string )
    {
        this.app = express();
        this.settings();
        this.middleware();
        this.routes();
    }

    settings()
    {
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
            res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
            next();
        });
        this.app.set('port', this.port || process.env.PORT || 3000);
    }

    middleware()
    {
        this.app.use(morgan('dev'));
        this.app.use(express.json());

    }

    routes()
    {
        this.app.use(IndexRoutes);
        this.app.use('/login', LoginRoutes );
        this.app.use('/alumnos', AlumnoRoutes ); 
        this.app.use('/usuarios',UsuarioRoutes);
        this.app.use('/egresos', EgresoRoutes);
        this.app.use('/ingresos' ,IngresoRoutes);
        this.app.use('/profesores', ProfesorRoutes);
        this.app.use('/areas', AreaRoutes);
        this.app.use('/horario', HorarioRoutes);
        this.app.use('/inventario', InventarioRoutes);
        this.app.use('/clases', ClasesRoutes);
        this.app.use('/Disciplina', DisciplinaRoutes);    
        this.app.use('/Impartir', ImpartirRoutes);
        this.app.use('/DetalleMaterial',DetalleMaterial);
        this.app.use('/Clases_has_Disciplinas',Clases_has_Disciplinas)
    }   
    
    async listen()
    {
        await this.app.listen(this.app.get('port'));
        console.log('Server on port', 3000);
    }

 
}

