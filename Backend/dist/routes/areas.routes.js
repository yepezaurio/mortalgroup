"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const areas_controller_1 = require("../controllers/areas.controller");
router.route('/nuevaArea')
    .post(areas_controller_1.postArea);
exports.default = router;
//# sourceMappingURL=areas.routes.js.map