"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const alumno_controller_1 = require("../controllers/alumno.controller");
router.route('/')
    .post(alumno_controller_1.postAlumno)
    //Este obtiene ahorita a todas las personas es un metodo que nos puede servir más tarde
    //En caso de que no Borrar
    .get(alumno_controller_1.getAlumnos);
router.route('/persona')
    .post(alumno_controller_1.postPersona);
router.route('/telefono')
    .post(alumno_controller_1.postTelefono);
router.route('/descuento')
    .post(alumno_controller_1.postDescuento)
    .get(alumno_controller_1.getDescuentos);
exports.default = router;
//# sourceMappingURL=alumno.routes.js.map