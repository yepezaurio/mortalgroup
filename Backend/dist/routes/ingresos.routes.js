"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const ingresos_controller_1 = require("../controllers/ingresos.controller");
router.route('/nuevoIngreso')
    .post(ingresos_controller_1.postIngreso);
exports.default = router;
//# sourceMappingURL=ingresos.routes.js.map