"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const inventario_controller_1 = require("../controllers/inventario.controller");
router.route('/nuevoInventario')
    .post(inventario_controller_1.postInventario);
exports.default = router;
//# sourceMappingURL=inventario.routes.js.map