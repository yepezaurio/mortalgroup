"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const egresos_controller_1 = require("../controllers/egresos.controller");
router.route('/')
    .post(egresos_controller_1.postEgreso);
exports.default = router;
//# sourceMappingURL=egresos.routes.js.map