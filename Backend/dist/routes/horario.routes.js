"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const horario_controller_1 = require("../controllers/horario.controller");
router.route('/nuevoHorario')
    .post(horario_controller_1.postHorario);
exports.default = router;
//# sourceMappingURL=horario.routes.js.map