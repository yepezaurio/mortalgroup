"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const impartir_controller_1 = require("../controllers/impartir.controller");
router.route('/')
    .post(impartir_controller_1.postImpartir);
exports.default = router;
//# sourceMappingURL=impartir.routes.js.map