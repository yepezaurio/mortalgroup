"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const Disciplina_controller_1 = require("../controllers/Disciplina.controller");
router.route('/')
    .post(Disciplina_controller_1.postDisciplina);
exports.default = router;
//# sourceMappingURL=Disciplinas.routes.js.map