"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const profesores_controller_1 = require("../controllers/profesores.controller");
router.route('/nuevoProfesor')
    .post(profesores_controller_1.postProfesores);
exports.default = router;
//# sourceMappingURL=profesores.routes.js.map