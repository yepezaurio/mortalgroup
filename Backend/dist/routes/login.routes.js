"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
//Importar nuestras funciones de nuestro controllador
const login_controller_1 = require("../controllers/login.controller");
//Ruta con la que vamos ingresar a las funciones 
router.route('/')
    .post(login_controller_1.postLogin);
//metodo por el cual lo vamos a consultar y pues la función
exports.default = router;
//# sourceMappingURL=login.routes.js.map