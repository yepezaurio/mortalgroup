"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const DetalleMaterial_controller_1 = require("../controllers/DetalleMaterial.controller");
router.route('/')
    .post(DetalleMaterial_controller_1.postDetalleMaterial);
exports.default = router;
//# sourceMappingURL=DetalleMaterial.routes.js.map