"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const Clases_has_Disciplinas_controller_1 = require("../controllers/Clases_has_Disciplinas.controller");
router.route('/')
    .post(Clases_has_Disciplinas_controller_1.postClases_has_Disciplinas);
exports.default = router;
//# sourceMappingURL=Clases_has_Disciplinas.routes.js.map