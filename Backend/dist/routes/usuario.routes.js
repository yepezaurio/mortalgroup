"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
//import {getAlumnos, postAlumno} from '../controllers/alumno.controller'
const usuario_controller_1 = require("../controllers/usuario.controller");
router.route('/')
    .post(usuario_controller_1.postUsuario);
exports.default = router;
//# sourceMappingURL=usuario.routes.js.map