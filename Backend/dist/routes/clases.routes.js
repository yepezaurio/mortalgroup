"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const clases_controller_1 = require("../controllers/clases.controller");
router.route('/nuevaClase')
    .post(clases_controller_1.postClase);
exports.default = router;
//# sourceMappingURL=clases.routes.js.map