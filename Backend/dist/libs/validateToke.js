"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function TokenValidation(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            if (!req.headers.authorization) {
                return res.status(401).send('Acceso denegado!');
            }
            let token = req.headers.authorization.split(' ')[1];
            if (token === 'null') {
                return res.status(401).send('Acceso denegado!');
            }
            const payload = yield jsonwebtoken_1.default.verify(token, process.env.TOKEN_USERS || 'tokenprueba');
            if (!payload) {
                return res.status(401).send('Acceso denegado!');
            }
            req.idUsuario = payload.id;
            next();
        }
        catch (e) {
            //console.log(e)
            return res.status(401).send('Acceso denegado!');
        }
    });
}
exports.TokenValidation = TokenValidation;
//# sourceMappingURL=validateToke.js.map