"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
function postDetalleMaterial(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newDetalleMaterial = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO DetalleMaterial SET ?', [newDetalleMaterial]);
        return res.json({
            message: 'Este es el detalle Material'
        });
    });
}
exports.postDetalleMaterial = postDetalleMaterial;
function postDisciplina(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newDisciplina = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO disciplinas SET ?', [newDisciplina]);
        return res.json({
            message: 'Se ha creado la Disciplina'
        });
    });
}
exports.postDisciplina = postDisciplina;
//# sourceMappingURL=DetalleMaterial.controller.js.map