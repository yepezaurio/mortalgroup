"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
function postHorario(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newHorario = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO Horarios SET ?', [newHorario]);
        res.json({
            message: 'Se ha creado con exito'
        });
    });
}
exports.postHorario = postHorario;
/**
 *
 *
 * insercion a la bd por el metodo post con un archivo json a la ruta /horario/nuevoHorario
 *
    {
    "HoraClase": "16:00",
    "DiasClase": "lunes, miercoles y jueves",
    "Profesores_idProfesores": 1
    }
 *
 *
 */ 
//# sourceMappingURL=horario.controller.js.map