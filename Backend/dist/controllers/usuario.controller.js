"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
//SingUp
function postUsuario(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newUsuario = req.body;
        const crypto = require('crypto');
        newUsuario.Nombre = crypto.createHash('md5').update(newUsuario.Nombre).digest('hex');
        newUsuario.Contrasena = crypto.createHash('md5').update(newUsuario.Contrasena).digest('hex');
        const conn = yield database_1.connect();
        conn.query('INSERT INTO Usuario SET ?', [newUsuario]);
        //token
        const token = jsonwebtoken_1.default.sign({ _id: newUsuario.idUsuario }, process.env.TOKEN_USER || 'tokenprueba');
        return res.header('authorization', token).json('Usuario Creado con Exito');
    });
}
exports.postUsuario = postUsuario;
function postPersona(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newPersona = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO Persona SET ?', [newPersona]);
        return res.json({
            message: 'Sa ha creado una persona con exito'
        });
    });
}
exports.postPersona = postPersona;
// Este metodo Devuelve todas las Usuario.
//SignIn
function getUsuario(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const conn = yield database_1.connect();
        const usuario = yield conn.query('Select * Usuarios');
        return res.json(usuario[0]);
    });
}
exports.getUsuario = getUsuario;
//# sourceMappingURL=usuario.controller.js.map