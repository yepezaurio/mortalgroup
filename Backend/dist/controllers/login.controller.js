"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
// La funcion para registrar está en Usuarios, por que allá se dan de altá
//Funcion para Hacer un Logeo en el Login.html 
function postLogin(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const conn = yield database_1.connect();
        const crypto = require('crypto');
        req.body.Nombre = crypto.createHash('md5').update(req.body.Nombre).digest('hex');
        req.body.Contrasena = crypto.createHash('md5').update(req.body.Contrasena).digest('hex');
        const Logeo = yield conn.query('SELECT * from Usuario Where Nombre= ? and Contraseña = ? ', [req.body.Nombre, req.body.Contrasena]);
        const JSON1 = JSON.parse(JSON.stringify(Logeo));
        const dato = JSON1[0].length;
        if (dato == 0) {
            return res.status(400).json('Usuario o Contraseña Invalidos');
        }
        else {
            const token = jsonwebtoken_1.default.sign({ id: JSON1.idUsuario }, process.env.TOKEN_USERS || 'tokenprueba', {
                expiresIn: 60 * 60 * 24
            });
            return res.header('authorization', token).json({ token });
        }
    });
}
exports.postLogin = postLogin;
//# sourceMappingURL=login.controller.js.map