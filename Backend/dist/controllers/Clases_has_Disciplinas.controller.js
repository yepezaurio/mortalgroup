"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
function postClases_has_Disciplinas(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newClases_has_Disciplinas = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO Clases_has_Disciplinas SET ?', [newClases_has_Disciplinas]);
        return res.json({
            message: 'la clase funciona'
        });
    });
}
exports.postClases_has_Disciplinas = postClases_has_Disciplinas;
//# sourceMappingURL=Clases_has_Disciplinas.controller.js.map