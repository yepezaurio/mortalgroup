"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
//Crea los datos de un Alumno
function postAlumno(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newAlumno = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO Alumnos SET ?', [newAlumno]);
        return res.json({
            message: 'Se ha creado alumno con exito'
        });
    });
}
exports.postAlumno = postAlumno;
//Crea los datos de una Persona.
function postPersona(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newPersona = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO Persona SET ?', [newPersona]);
        const idPersona = yield conn.query('select idPersona from Persona order by idPersona desc limit 1');
        return res.json(idPersona[0]);
    });
}
exports.postPersona = postPersona;
// Crea un Telefono.
function postTelefono(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newTelefono = req.body;
        const conn = yield database_1.connect();
        yield conn.query('INSERT INTO Telefonos SET ?', [newTelefono]);
        const idTelefono = yield conn.query('select idTelefonos from Telefonos order by idTelefonos desc limit 1;');
        return res.json(idTelefono[0]);
    });
}
exports.postTelefono = postTelefono;
// Crea un descuento 
function postDescuento(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newDescuento = req.body;
        const conn = yield database_1.connect();
        conn.query('insert into Descuentos SET ?', [newDescuento]);
        return res.json({
            message: 'Se ha creado un Descuento con exito '
        });
    });
}
exports.postDescuento = postDescuento;
// Este metodo Devuelve todas las personas.
function getAlumnos(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const conn = yield database_1.connect();
        const alumnos = yield conn.query('Select * From Persona ');
        return res.json(alumnos[0]);
    });
}
exports.getAlumnos = getAlumnos;
function getDescuentos(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const conn = yield database_1.connect();
        const nombreDesc = yield conn.query('Select * from Descuentos');
        return res.json(nombreDesc[0]);
    });
}
exports.getDescuentos = getDescuentos;
//# sourceMappingURL=alumno.controller.js.map