"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../database");
function postImpartir(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const newImpartir = req.body;
        const conn = yield database_1.connect();
        conn.query('INSERT INTO Impartir SET ?', [newImpartir]);
        return res.json({
            message: 'Se ha podido asignar clase'
        });
    });
}
exports.postImpartir = postImpartir;
/**
 export async function postDisciplina (req: Request, res: Response)
{
const newDisciplina: Disciplinas = req.body;
const conn = await connect();
conn.query('INSERT INTO Disciplina SET ?', [newDisciplina]);
return res.json({
    message: 'Se ha creado el Usuario'
})
 
}



*/
/**
 export async function postProfesor (req: Request, res: Response)
{
const newProfesor: Profesor = req.body;
const conn = await connect();
conn.query('INSERT INTO Profesor SET ?', [newProfesor]);
return res.json({
    message: 'Se ha creado el Usuario'
})
 
}
*/
//# sourceMappingURL=impartir.controller.js.map