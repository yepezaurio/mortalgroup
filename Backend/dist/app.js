"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
//Routes Importante para saber las rutas 
const index_routes_1 = __importDefault(require("./routes/index.routes"));
const login_routes_1 = __importDefault(require("./routes/login.routes"));
const alumno_routes_1 = __importDefault(require("./routes/alumno.routes"));
const usuario_routes_1 = __importDefault(require("./routes/usuario.routes"));
const egresos_routes_1 = __importDefault(require("./routes/egresos.routes"));
const ingresos_routes_1 = __importDefault(require("./routes/ingresos.routes"));
const profesores_routes_1 = __importDefault(require("./routes/profesores.routes"));
const areas_routes_1 = __importDefault(require("./routes/areas.routes"));
const horario_routes_1 = __importDefault(require("./routes/horario.routes"));
const inventario_routes_1 = __importDefault(require("./routes/inventario.routes"));
const impartir_routes_1 = __importDefault(require("./routes/impartir.routes"));
const Disciplinas_routes_1 = __importDefault(require("./routes/Disciplinas.routes"));
const clases_routes_1 = __importDefault(require("./routes/clases.routes"));
const DetalleMaterial_routes_1 = __importDefault(require("./routes/DetalleMaterial.routes"));
const Clases_has_Disciplinas_routes_1 = __importDefault(require("./routes/Clases_has_Disciplinas.routes"));
class App {
    constructor(port) {
        this.port = port;
        this.app = express_1.default();
        this.settings();
        this.middleware();
        this.routes();
    }
    settings() {
        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
            res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
            next();
        });
        this.app.set('port', this.port || process.env.PORT || 3000);
    }
    middleware() {
        this.app.use(morgan_1.default('dev'));
        this.app.use(express_1.default.json());
    }
    routes() {
        this.app.use(index_routes_1.default);
        this.app.use('/login', login_routes_1.default);
        this.app.use('/alumnos', alumno_routes_1.default);
        this.app.use('/usuarios', usuario_routes_1.default);
        this.app.use('/egresos', egresos_routes_1.default);
        this.app.use('/ingresos', ingresos_routes_1.default);
        this.app.use('/profesores', profesores_routes_1.default);
        this.app.use('/areas', areas_routes_1.default);
        this.app.use('/horario', horario_routes_1.default);
        this.app.use('/inventario', inventario_routes_1.default);
        this.app.use('/clases', clases_routes_1.default);
        this.app.use('/Disciplina', Disciplinas_routes_1.default);
        this.app.use('/Impartir', impartir_routes_1.default);
        this.app.use('/DetalleMaterial', DetalleMaterial_routes_1.default);
        this.app.use('/Clases_has_Disciplinas', Clases_has_Disciplinas_routes_1.default);
    }
    listen() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.app.listen(this.app.get('port'));
            console.log('Server on port', 3000);
        });
    }
}
exports.App = App;
//# sourceMappingURL=app.js.map