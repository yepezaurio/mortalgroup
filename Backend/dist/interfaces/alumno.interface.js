"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *
 * Creación de JSON para crear alumno con metodo POST
 * en la ruta /alumnos
    {
    "CURP": "LOYM8450LASDPR0",
    "Estatura": 175,
    "Peso": 80,
    "TipoSangre": "A+",
    "NombreTutor": null,
    "NombrePapa": null,
    "NombreMama": null,
    "TallaAlum": "M",
    "TallaCintura": 90,
    "Descuentos_idDescuentos": 1,
    "Telefonos_idTelefonos": 1,
    "Persona_idPersona": 1
    }
 */ 
//# sourceMappingURL=alumno.interface.js.map