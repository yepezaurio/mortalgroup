"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *
 *  Código tipo JSON solo para copiar y pegar y hacer pruebas en insomnia en el metodo POST
 *  hacia la tabla de egresos.
 *
 * **************************************************
        {
            "TipoEgreso": "Renta",
            "Monto": 350,
            "Descripcion": "Pago de renta del lugar"
        }
* ***************************************************
*/ 
//# sourceMappingURL=egresos.interface.js.map