import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-descuentos',
  templateUrl: './descuentos.component.html',
  styleUrls: ['./descuentos.component.css']
})
export class DescuentosComponent implements OnInit {

  constructor(private http: HttpClient) { }

  descuento = {};

  ngOnInit() {
  }
  
  crearDesc()
  {
    console.log(this.descuento)
    this.http.post('http://localhost:3000/alumnos/descuento', this.descuento).toPromise().then( ( data: any ) => {
      console.log(data);
    });
    //this.http.post('http://localhost:3000/alumnos/', this.alumnos);
  }
}
