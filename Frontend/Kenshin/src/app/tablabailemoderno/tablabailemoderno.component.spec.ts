import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablabailemodernoComponent } from './tablabailemoderno.component';

describe('TablabailemodernoComponent', () => {
  let component: TablabailemodernoComponent;
  let fixture: ComponentFixture<TablabailemodernoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablabailemodernoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablabailemodernoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
