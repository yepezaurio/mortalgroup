import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablacalisteniaComponent } from './tablacalistenia.component';

describe('TablacalisteniaComponent', () => {
  let component: TablacalisteniaComponent;
  let fixture: ComponentFixture<TablacalisteniaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablacalisteniaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablacalisteniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
