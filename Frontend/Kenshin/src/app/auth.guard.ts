import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(
      private autService: AuthService,
      private router: Router
    
      ){ 

    }

  canActivate(): boolean{
    if(this.autService.loggedIn()){
      return true; 
    }

    this.router.navigate(['/login'])
  }
  
}
  

