import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablatelefonosComponent } from './tablatelefonos.component';

describe('TablatelefonosComponent', () => {
  let component: TablatelefonosComponent;
  let fixture: ComponentFixture<TablatelefonosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablatelefonosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablatelefonosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
