import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabladescuentosComponent } from './tabladescuentos.component';

describe('TabladescuentosComponent', () => {
  let component: TabladescuentosComponent;
  let fixture: ComponentFixture<TabladescuentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabladescuentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabladescuentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
