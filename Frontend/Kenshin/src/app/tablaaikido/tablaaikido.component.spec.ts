import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaaikidoComponent } from './tablaaikido.component';

describe('TablaaikidoComponent', () => {
  let component: TablaaikidoComponent;
  let fixture: ComponentFixture<TablaaikidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaaikidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaaikidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
