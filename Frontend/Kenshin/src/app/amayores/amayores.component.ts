import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-amayores',
  templateUrl: './amayores.component.html',
  styleUrls: ['./amayores.component.css']
})

export class AmayoresComponent implements OnInit {

  constructor(private http: HttpClient) { }

  persona = {};
  alumnos = {
    "Persona_idPersona": "",
    "Telefonos_idTelefonos": ""
  };
  telefonos = {};
  //descuento = {};
  disciplinas = {};
  private Persona_idPersona = "";
  private Telefono_idTelefono = "";
  private URL = 'http://localhost:3000/alumnos';
  
  Descuento : any = [];
  ngOnInit() {

    // Aquí obtengo todos los registros de descuento guardado en la variable Descuento, y se hace esta consulta en cuanto entro
    // a la página de Insertar Alumnos. 
    this.http.get(this.URL + '/descuento').subscribe(res =>{ 
      this.Descuento = res; }, 
      err => console.log(err));
  }
  
  async registro()
  {
    //Aquí hago post a persona y me debe de devolver el ID de la persona que acabamos de Añadir
    //El ID Lo añadimos a la variable Telefonos_idTelefonos
    const response = await Promise.all([
      this.http.post(this.URL + "/persona", this.persona).toPromise(),
      this.http.post(this.URL + '/telefono', this.telefonos).toPromise()
    ]);
  
    this.alumnos.Persona_idPersona = response[0][0].idPersona  ; 
    this.alumnos.Telefonos_idTelefonos = response[1][0].idTelefonos;
    /*
    this.http.post(this.URL + "/persona", this.persona).toPromise().then( (data: any ) => {
      console.log(data[0].idPersona);
      this.alumnos.Persona_idPersona = data[0].idPersona ; 
    });
    //Aquí hago post a Telefonos y me debe de devolver el ID del telefono que añadimos.
    //El ID Lo añadimos a la variable Telefonos_idTelefonos
    this.http.post(this.URL + '/telefono', this.telefonos).toPromise().then( (data: any ) => {
      console.log(data);
      this.alumnos.Telefonos_idTelefonos = data[0].idTelefonos;
    });
    */
    // Hacemos post a un alumnos
    this.http.post(this.URL , this.alumnos).toPromise()
    .then( ( data: any) =>{
      console.log(data);
    });
    
    
  }

}













