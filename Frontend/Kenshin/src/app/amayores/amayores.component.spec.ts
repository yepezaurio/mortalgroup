import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmayoresComponent } from './amayores.component';

describe('AmayoresComponent', () => {
  let component: AmayoresComponent;
  let fixture: ComponentFixture<AmayoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmayoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmayoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
