import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Aquí van el modulo que creamos normalmente va por defetco 

//Angular Pages
import { LoginComponent } from './login/login.component';

//Utilidades 
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { AmayoresComponent } from './amayores/amayores.component';
import { InscribiralumnoComponent } from './inscribiralumno/inscribiralumno.component';
import { RecibirpagoComponent } from './recibirpago/recibirpago.component';
import { DescuentosComponent } from './descuentos/descuentos.component';
import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { RegistrarNominaComponent } from './registrar-nomina/registrar-nomina.component';
import { NuevoPedidoComponent } from './nuevo-pedido/nuevo-pedido.component';
import { ClasesComponent } from './clases/clases.component';
import { DisciplinasComponent } from './disciplinas/disciplinas.component';
import { HorariosComponent } from './horarios/horarios.component';
import { TablaaikidoComponent } from './tablaaikido/tablaaikido.component';
import { TablabailemodernoComponent } from './tablabailemoderno/tablabailemoderno.component';
import { TablacalisteniaComponent } from './tablacalistenia/tablacalistenia.component';
import { tablaclasesComponent } from './tablaclases/tablaclases.component';
import { TabladisciplinasComponent } from './tabladisciplinas/tabladisciplinas.component';
import { TablagimnaciaComponent } from './tablagimnacia/tablagimnacia.component';
import { TablahorariosComponent } from './tablahorarios/tablahorarios.component';
import { TablanominasComponent } from './tablanominas/tablanominas.component';
import { TablapersonasComponent } from './tablapersonas/tablapersonas.component';
import { TablareportesComponent } from './tablareportes/tablareportes.component';
import { TablataekwondoComponent } from './tablataekwondo/tablataekwondo.component';
import { TablatelefonosComponent } from './tablatelefonos/tablatelefonos.component';
import { TelefonosComponent } from './telefonos/telefonos.component';
import { AreasComponent } from './areas/areas.component';
import { TabladescuentosComponent } from './tabladescuentos/tabladescuentos.component';
import { TablaareasComponent } from './tablaareas/tablaareas.component';
import { TablaempleadosComponent } from './tablaempleados/tablaempleados.component';
import { TablaalumnosComponent } from './tablaalumnos/tablaalumnos.component';
import { InfoCostosComponent } from './info-costos/info-costos.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AmayoresComponent,
    InscribiralumnoComponent,
    RecibirpagoComponent,
    DescuentosComponent,
    RegistrarNominaComponent,
    NuevoPedidoComponent,
    ClasesComponent,
    DisciplinasComponent,
    HorariosComponent,
    TablaaikidoComponent,
    TablabailemodernoComponent,
    TablacalisteniaComponent,
    tablaclasesComponent,
    TabladisciplinasComponent,
    TablagimnaciaComponent,
    TablahorariosComponent,
    TablanominasComponent,
    TablapersonasComponent,
    TablareportesComponent,
    TablataekwondoComponent,
    TablatelefonosComponent,
    TelefonosComponent,
    AreasComponent,
    DescuentosComponent,
    TabladescuentosComponent,
    TablaareasComponent,
    TablaempleadosComponent,
    TablaalumnosComponent,
    InfoCostosComponent
  ],
  imports: [
    //Aqui importamos todos las utilidades 
    BrowserModule,
    AppRoutingModule,
    HttpClientModule, 
    FormsModule
  ],
  providers: [ 
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true   
    } 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
