import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablanominasComponent } from './tablanominas.component';

describe('TablanominasComponent', () => {
  let component: TablanominasComponent;
  let fixture: ComponentFixture<TablanominasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablanominasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablanominasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
