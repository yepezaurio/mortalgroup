import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaareasComponent } from './tablaareas.component';

describe('TablaareasComponent', () => {
  let component: TablaareasComponent;
  let fixture: ComponentFixture<TablaareasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaareasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
