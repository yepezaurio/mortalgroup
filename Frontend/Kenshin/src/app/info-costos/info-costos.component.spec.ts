import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoCostosComponent } from './info-costos.component';

describe('InfoCostosComponent', () => {
  let component: InfoCostosComponent;
  let fixture: ComponentFixture<InfoCostosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoCostosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoCostosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
