import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaalumnosComponent } from './tablaalumnos.component';

describe('TablaalumnosComponent', () => {
  let component: TablaalumnosComponent;
  let fixture: ComponentFixture<TablaalumnosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaalumnosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaalumnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
