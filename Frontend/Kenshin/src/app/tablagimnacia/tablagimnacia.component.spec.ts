import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablagimnaciaComponent } from './tablagimnacia.component';

describe('TablagimnaciaComponent', () => {
  let component: TablagimnaciaComponent;
  let fixture: ComponentFixture<TablagimnaciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablagimnaciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablagimnaciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
