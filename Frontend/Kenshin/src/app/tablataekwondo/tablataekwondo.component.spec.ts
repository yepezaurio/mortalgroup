import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablataekwondoComponent } from './tablataekwondo.component';

describe('TablataekwondoComponent', () => {
  let component: TablataekwondoComponent;
  let fixture: ComponentFixture<TablataekwondoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablataekwondoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablataekwondoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
