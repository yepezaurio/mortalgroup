import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabladisciplinasComponent } from './tabladisciplinas.component';

describe('TabladisciplinasComponent', () => {
  let component: TabladisciplinasComponent;
  let fixture: ComponentFixture<TabladisciplinasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabladisciplinasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabladisciplinasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
