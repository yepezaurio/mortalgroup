import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablahorariosComponent } from './tablahorarios.component';

describe('TablahorariosComponent', () => {
  let component: TablahorariosComponent;
  let fixture: ComponentFixture<TablahorariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablahorariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablahorariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
