import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaclasesComponent } from './tablaclases.component';

describe('TablaclasesComponent', () => {
  let component: TablaclasesComponent;
  let fixture: ComponentFixture<TablaclasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaclasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaclasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
