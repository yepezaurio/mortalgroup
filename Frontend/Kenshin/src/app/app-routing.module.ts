import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AmayoresComponent } from './amayores/amayores.component';
import { DescuentosComponent } from './descuentos/descuentos.component';
import { AuthGuard } from './auth.guard';
import { InscribiralumnoComponent } from './inscribiralumno/inscribiralumno.component';
import { RecibirpagoComponent } from './recibirpago/recibirpago.component';
import { NuevoPedidoComponent } from './nuevo-pedido/nuevo-pedido.component';
import { RegistrarNominaComponent } from './registrar-nomina/registrar-nomina.component';
import { ClasesComponent } from './clases/clases.component';
import { DisciplinasComponent } from './disciplinas/disciplinas.component';
import { HorariosComponent } from './horarios/horarios.component';
import { TablaaikidoComponent } from './tablaaikido/tablaaikido.component';
import { TablabailemodernoComponent } from './tablabailemoderno/tablabailemoderno.component';
import { TablacalisteniaComponent } from './tablacalistenia/tablacalistenia.component';
import { tablaclasesComponent } from './tablaclases/tablaclases.component';
import { TabladisciplinasComponent } from './tabladisciplinas/tabladisciplinas.component';
import { TablagimnaciaComponent } from './tablagimnacia/tablagimnacia.component';
import { TablahorariosComponent } from './tablahorarios/tablahorarios.component';
import { TablanominasComponent } from './tablanominas/tablanominas.component';
import { TablapersonasComponent } from './tablapersonas/tablapersonas.component';
import { TablareportesComponent } from './tablareportes/tablareportes.component';
import { TablataekwondoComponent } from './tablataekwondo/tablataekwondo.component';
import { TablatelefonosComponent } from './tablatelefonos/tablatelefonos.component';
import { TelefonosComponent } from './telefonos/telefonos.component';
import { AreasComponent } from './areas/areas.component';
import { TabladescuentosComponent } from './tabladescuentos/tabladescuentos.component';
import { TablaareasComponent } from './tablaareas/tablaareas.component';
import { TablaempleadosComponent } from './tablaempleados/tablaempleados.component';
import { TablaalumnosComponent } from './tablaalumnos/tablaalumnos.component';
import { InfoCostosComponent } from './info-costos/info-costos.component';
import { from } from 'rxjs';


//Aqui se agregan todas las rutas nuevas para poder hacer la páginación.
//El path representa como se va mostrar en el url. Ej /login
const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'}, 
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'amayores', component: AmayoresComponent, canActivate: [AuthGuard]}, 
  { path: 'inscribiralumno', component: InscribiralumnoComponent, canActivate: [AuthGuard] },
  { path: 'recibirpago', component: RecibirpagoComponent, canActivate: [AuthGuard]},
  { path: 'descuentos', component: DescuentosComponent, canActivate: [AuthGuard]},
  { path: 'registrar-nomina', component: RegistrarNominaComponent, canActivate: [AuthGuard]},
  { path: 'nuevo-pedido', component: NuevoPedidoComponent, canActivate: [AuthGuard]},
  { path: 'clases', component: ClasesComponent, canActivate: [AuthGuard] } ,
  { path: 'disciplinas', component: DisciplinasComponent, canActivate: [AuthGuard] } ,
  { path: 'horarios', component: HorariosComponent, canActivate: [AuthGuard] } ,
  { path: 'tablaaikido', component: TablaaikidoComponent, canActivate: [AuthGuard] } , 
  { path: 'tablabailemoderno', component: TablabailemodernoComponent, canActivate: [AuthGuard] } ,
  { path: 'tablacalistenia', component: TablacalisteniaComponent, canActivate: [AuthGuard] } ,
  { path: 'tablaclases', component: tablaclasesComponent, canActivate: [AuthGuard] }  ,
  { path: 'tabladisciplinas', component: TabladisciplinasComponent, canActivate: [AuthGuard] } ,
  { path: 'tablagimnasia', component: TablagimnaciaComponent, canActivate: [AuthGuard] } ,
  { path: 'tablahorarios', component: TablahorariosComponent, canActivate: [AuthGuard] }  ,
  { path: 'tablanominas', component: TablanominasComponent, canActivate: [AuthGuard] } ,
  { path: 'tablapersonas', component: TablapersonasComponent, canActivate: [AuthGuard] } ,
  { path: 'tablareportes', component: TablareportesComponent, canActivate: [AuthGuard] } ,
  { path: 'tablataekwondo', component: TablataekwondoComponent, canActivate: [AuthGuard] } ,
  { path: 'tablatelefonos', component: TablatelefonosComponent, canActivate: [AuthGuard] } ,
  { path: 'telefonos', component: TelefonosComponent, canActivate: [AuthGuard] } ,
  { path: 'areas', component: AreasComponent , canActivate: [AuthGuard]} ,
  { path: 'tablaalumnos', component: TablaalumnosComponent, canActivate: [AuthGuard] } ,
  { path: 'tablaempleados', component: TablaempleadosComponent, canActivate: [AuthGuard] },
  { path: 'tabladescuentos', component: TabladescuentosComponent, canActivate: [AuthGuard] },
  { path: 'tablaareas', component: TablaareasComponent, canActivate: [AuthGuard] },
  { path: 'infocostos', component: InfoCostosComponent, canActivate: [AuthGuard] } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
